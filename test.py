import os
from selenium import webdriver
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from threading import Thread
# This array 'capabilities' defines the capabilities browser, device and OS combinations where the test will run
BUILD_NAME = "browserstack-build-2"

capabilities = [
    {
        "browserName": "chrome",
        "browserVersion": "104.0 beta",
        "os": "Windows",
        "osVersion": "11",
        "sessionName": "Depresion", # test name
        "buildName": BUILD_NAME,  # Your tests will be organized within this build
    },
    {
        "browserName": "firefox",
        "browserVersion": "104.0 beta",
        "os": "Windows",
        "osVersion": "11",
        "sessionName": "Video de perrito que se nos va",
        "buildName": BUILD_NAME,
    }
]

def get_browser_option(browser):
    
    switcher = {
        "chrome": ChromeOptions(),
        "firefox": FirefoxOptions(),
    }

    return switcher.get(browser, ChromeOptions())

def run_session(cap):
    cap["userName"] = os.environ.get("BROWSERSTACK_USERNAME") or "martnmiranda_nBdHPX"
    cap["accessKey"] = os.environ.get("BROWSERSTACK_ACCESS_KEY") or "kiyXF1ubqhXmAhBsidzT"
    URL = "https://" + cap["userName"] + ":" + cap["accessKey"]  + "@hub-cloud.browserstack.com/wd/hub";
    options = get_browser_option(cap["browserName"].lower())
    options.set_capability("browserName", cap["browserName"].lower())
    options.set_capability("bstack:options", cap)
    driver = webdriver.Remote(
        command_executor=URL, options=options
    )
    driver.get("https://poliwork.onrender.com/")
    driver.maximize_window()
    delay = 20
    myElem = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, 'LOGIN_BUTTON')))
    login = driver.find_element(By.ID, "LOGIN_BUTTON")
    login.click()
    singup = driver.find_element(By.ID, "SINGUP_LINK")
    singup.click()
    print("something")
    driver.quit()

for cap in capabilities:
    Thread(target=run_session, args=(cap,)).start()