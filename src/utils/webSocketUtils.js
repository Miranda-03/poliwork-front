import io from "socket.io-client";
import { SnackbarProvider, useSnackbar } from 'notistack';
import {Notification} from '../components/appbar'

export const socket = io(process.env.REACT_APP_CURRENT_URL_TO_API);
