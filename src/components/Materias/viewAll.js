import '@fontsource/roboto';
import * as React from 'react';
import { useEffect, useState, useContext } from "react";
import SingleMateria from './single';
import { Grid } from '@mui/material';
import axios from 'axios';
import CssBaseline from '@mui/material/CssBaseline';
import { Link } from 'react-router-dom';
import Button from '@mui/material/Button';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import { parseJwt } from '../../utils/cookiesUtils';
import { ViewMateriaContext } from './single';

let alumnoName = ""

export default function ViewAllMaterias() {

    const [materias, setMaterias] = useState([])    
    const [alignment, setAlignment] = React.useState('');
    const [disabledP, setDisableP] = useState(true)
    const [disabledT, setDisableT] = useState(true)


    const handleAlignment = (event, newAlignment) => {
        if (newAlignment !== null) {
            setAlignment(newAlignment);
        }
    };

    useEffect(() => {
        let propArray = []
        let todosArray = []
        axios.get(`${process.env.REACT_APP_CURRENT_URL_TO_API}/materias`)
            .then((receive) => {
                console.log(receive)
                setMaterias(receive.data.message)
               
    
           
                            
    })
},[])

    const ObtenerMateriasFunction = () => {
   
        return (materias.map((materia) => {
            console.log(materia)
            return <Grid item xs sm md key={materia._id}>

                <SingleMateria
                    nombre={materia.Nombre}
                    Profesor={materia.Profesor}
                    Alumno={materias.AlumnoCreador}
                    dias={materia.Dias}
                    
                />
                <Button
                        onClick={() => { handleDelete(materia) }}
                        size="small"
                        sx={{
                            width : "100%",
                            margin : "auto",
                            marginTop : "15px"
                        }}
                    >
                        Eliminar Materia
                        </Button>
                        <Button
                        onClick={() => { handleModificar(materia) }}
                        size="small"
                        sx={{
                            width : "100%",
                            margin : "auto",
                            marginTop : "15px"
                        }}
                    >
                        Modificar Materia
                        </Button>        
                

            </Grid>
        }))
    }
    
    const handleDelete = (materia) => {
        console.log(materia._id     )

            axios.delete(`${process.env.REACT_APP_CURRENT_URL_TO_API}/materias/${materia._id}`)
            .then((message) => {
                console.log(message)
            })
            .catch((error) => {
                console.log(error)
            })             
    }
    const handleModificar = (materia) => {
            
                
            axios.patch(`${process.env.REACT_APP_CURRENT_URL_TO_API}/materias/${materia._id}`)
            .then((message) => {
                console.log(message)
            })
            .catch((error) => {
                console.log(error)
            })             
    }


    return (
        <React.Fragment>
            <CssBaseline />
            <Grid
                container
                justifyContent="space-between"
                alignItems="center"
                direction="row"
                style={{ padding: "0 60px 0 60px" }}
            >
                <h1>Materias</h1>
                <ToggleButtonGroup
                    color="primary"
                    value={alignment}
                    exclusive
                    onChange={handleAlignment}
                    aria-label="Platform"
                >
                    <ToggleButton disabled={disabledT} value="todas">Todas</ToggleButton>
                   
                </ToggleButtonGroup>
                <Button
                    LinkComponent={Link} to={'/agregar'}
                    variant="contained"
                >
                    Agregar materia
                </Button>
            </Grid>
            <Grid
                container
                direction="row"
                justifyContent="flex-start"
                alignItems="flex-start"
                spacing={10}
                columns={{ xs: 1, sm: 6, md: 12 }}
                style={{ padding: "0 60px 60px 60px" }}
            >

                <ObtenerMateriasFunction />
                
            </Grid>
        </React.Fragment>
    );
    
}
