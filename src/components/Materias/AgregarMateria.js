import '@fontsource/roboto';
import * as React from 'react';
    import { useEffect, useState, useContext, useRef, createContext } from "react";
import dayjs from 'dayjs';
import TextField from '@mui/material/TextField';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Chip from '@mui/material/Chip';
import Paper from '@mui/material/Paper';
import Container from '@mui/material/Container';
import { styled } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { TimePicker } from '@mui/x-date-pickers/TimePicker';
import { parseJwt } from '../../utils/cookiesUtils';
import axios from 'axios';


const ListItem = styled('li')(({ theme }) => ({
    margin: theme.spacing(0.5),
}));

export default function StepThree() {

    const DescripcionReunion = useRef();
    const DiaMateria = useRef();
    const ProfesorMateria = useRef(); 
    const NombreMateria = useRef();
    const TituloReunion = useRef();
    const [HorarioInicio, setHorarioInicio] = useState([])
    const [HorarioFinal, setHorararioFinal] = useState([])
    const[CrearHorario, setCrearHorario] = useState([])
   
    const [ReunionObj, setReunionObj] = useState([])
    const [fechaReunion, setfechaReunion] = React.useState(new Date())

    const handleDelete = (chipToDelete) => () => {
        setReunionObj((chips) => chips.filter((chip) => chip !== chipToDelete));
    };
    const handleHorarios = () =>{
        const Inicio= new Date(HorarioInicio)
        const Final = new Date(HorarioFinal)
        let ObjetoHorario = {
            "inicio": Inicio,
            "final": Final
        }
        setCrearHorario(state => ({ ...state }))
        setCrearHorario(CrearHorario.concat(ObjetoHorario))

    }

    const handleAddDia = () => {
        const DatefechaReunion = new Date(fechaReunion)
        let OBJETOReunion = {
             "Dia": DiaMateria.current.value, "Horarios": CrearHorario
        }
        setReunionObj(state => ({ ...state }))
        setReunionObj(ReunionObj.concat(OBJETOReunion))
    }
    const handleAddMateria=() => {
        const data = parseJwt()
        if (data != undefined) {
            const alumno = data.usuario.name
           
        let body ={
            "Nombre": NombreMateria.current.value,
            "Profesor": ProfesorMateria.current.value,
            "Dias": ReunionObj,
            "AlumnoCreador":alumno
        }
        console.log(body);
        axios.post(`${process.env.REACT_APP_CURRENT_URL_TO_API}/materias`, body)
            .then((message) => {
                console.log(message)
            })
            .catch((error) => {
                console.log(error)
            })
        }

    }

    return (
        <React.Fragment>
            <CssBaseline />
            <Container maxWidth="sm">
                <Grid item xs={1} sm={2}>
                    <Paper
                        sx={{
                            display: 'flex',
                            justifyContent: 'center',
                            flexWrap: 'wrap',
                            listStyle: 'none',
                            p: 0.5,
                            m: 0,
                            marginBottom: "15px"
                        }}
                        component="ul"
                    >
                        {ReunionObj.map((data, index) => {
                            return (
                                <ListItem key={index}>
                                    <Chip
                                        label={data.tituloReunion}
                                        onDelete={handleDelete(data)}
                                    />
                                </ListItem>
                            );
                        })}
                    </Paper>
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                        <TimePicker
                            renderInput={(props) => <TextField {...props} />}
                            value={HorarioInicio}
                            label="Fecha inicio"
                            onChange={(newValue) => {
                                setHorarioInicio(newValue);
                            }}
                        />
                    </LocalizationProvider> 
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                        <TimePicker
                            renderInput={(props) => <TextField {...props} />}
                            value={HorarioFinal}

                            label="Fecha final"
                            onChange={(newValue) => {
                                setHorararioFinal(newValue);
                            }}
                        />
                    </LocalizationProvider>
                    <TextField
                        multiline
                        sx={{ width: "100%" }}
                        id="standard-basic"
                        inputRef={NombreMateria}
                        label="Nombre"
                        variant="standard"
                    />
                     <TextField
                        multiline
                        sx={{ width: "100%" }}
                        id="standard-basic"
                        inputRef={DiaMateria}
                        label="Dia"
                        variant="standard"
                    />
                      <TextField
                        multiline
                        sx={{ width: "100%" }}
                        id="standard-basic"
                        inputRef={ProfesorMateria}
                        label="Profesor"
                        variant="standard"
                    />
                   
                    
                    <Button
                        onClick={handleHorarios}
                        size="small"
                        sx={{
                            width : "100%",
                            margin : "auto",
                            marginTop : "15px"
                        }}
                    >
                       Elegir Horario
                    </Button>
                    <Button
                        onClick={handleAddDia}
                        size="small"
                        sx={{
                            width : "100%",
                            margin : "auto",
                            marginTop : "15px"
                        }}
                    >
                        Crear dia
                    </Button>
                    <Button
                        onClick={handleAddMateria}
                        size="small"
                        sx={{
                            width : "100%",
                            margin : "auto",
                            marginTop : "15px"
                        }}
                    >
                        Agregar Materia
                        </Button>
                </Grid>
            </Container>
        </React.Fragment>
    );

}