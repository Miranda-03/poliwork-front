import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import { useEffect, useState, createContext } from 'react';

import Typography from '@mui/material/Typography';
import { Link } from 'react-router-dom';

export const ViewMateriaContext = createContext([{}, () => { }]);
export default function SingleMateria({ nombre, alumno, profesor}) {
  return (
    <Card sx={{ minWidth: 300 }}>
      <CardContent>
        <Typography variant="h5" component="div">
          {nombre}
        </Typography>
        <Typography sx={{ mb: 1.5 }} color="text.secondary">
          {alumno}
        </Typography>
        <Typography variant="body2">
          {profesor}
        </Typography>
      </CardContent>
    </Card>
  );
}
