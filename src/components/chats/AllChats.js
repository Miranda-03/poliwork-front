import '@fontsource/roboto';
import * as React from 'react';
import axios from 'axios';
import { useState, useEffect, useRef, createContext } from "react";
import { parseJwt } from '../../utils/cookiesUtils';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import CssBaseline from '@mui/material/CssBaseline';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import MuiAlert, { AlertProps } from '@mui/material/Alert';
import { SnackbarProvider, useSnackbar } from 'notistack';
import DeleteIcon from '@mui/icons-material/Delete';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Divider from '@mui/material/Divider';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import ListItemButton from '@mui/material/ListItemButton';
import { FixedSizeList } from 'react-window';
import ImageIcon from '@mui/icons-material/Image';
import WorkIcon from '@mui/icons-material/Work';
import BeachAccessIcon from '@mui/icons-material/BeachAccess';
import ListItemIcon from '@mui/material/ListItemIcon';
import InboxIcon from '@mui/icons-material/Inbox';
import DraftsIcon from '@mui/icons-material/Drafts';
import Chat from './Chat'
import Grid from '@mui/material/Grid'; // Grid version 1
import { styled } from '@mui/material/styles';
import { height } from '@mui/system';
import Container from '@mui/material/Container';


let alumnoName;

export const AllChatContext = createContext([]);

export default function AllChats() {

    const [selectedIndex, setSelectedIndex] = React.useState("");
    const [amigos, setAmigos] = useState([])
    const [alumnoId, setAlumnoId] = useState()

    const handleListItemClick = (event, index) => {
        setSelectedIndex(index);
    };

    const handleID = (number) => {
        setAlumnoId(number)
    }

    useEffect(() => {
        const setNotAndMails = () => {
            const data = parseJwt()
            if (data != undefined) {
                const alumno = data.usuario
                handleID(alumno.name)
            }
        }
        setNotAndMails()
    }, [])

    const values = {

        "SelectedUser": selectedIndex,
        "userClient": alumnoId

    }

    const Item = styled(Paper)(({ theme }) => ({
        backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
        ...theme.typography.body2,
        padding: theme.spacing(1),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    }));


    useEffect(() => {
        let arrayAmigos = []
        const data = parseJwt()
        if (data != undefined) alumnoName = data.usuario.name
        const options = {
            withCredentials: true
        }
        axios.get(`${process.env.REACT_APP_CURRENT_URL_TO_API}/alumnos/`, options)
            .then((response) => {
                const alumnos = response.data.message
                alumnos.map((alumno) => {
                    if (alumno.Amigos != undefined) {
                        alumno.Amigos.map((amigo) => {
                            if (amigo == alumnoName) arrayAmigos.push(alumno)
                        })
                    }
                })
                setAmigos(arrayAmigos)
            })
            .catch((error) => {
                console.log(error.response.data)
            })
    }, [])

    return (
        <AllChatContext.Provider value={values}>
            <React.Fragment >
                <SnackbarProvider maxSnack={3}>
                    <CssBaseline />
                    <div class="body row scroll-y">
                        <Paper elevation={0}
                            sx={{
                                width: "1000px",
                                margin: "auto",
                                height: "100%"
                            }}

                            height="100vh" display="flex" flexDirection="column"
                        >
                            <Grid container
                                direction="row"
                                justifyContent="center"
                                alignItems="stretch"
                                sx={{ width: "100%", flexWrap: "nowrap", height: "inherit" }}
                            >

                                <Item sx={{ width: "40%", marginRight: "10px" }}>
                                    <Box >
                                        <List component="nav" aria-label="secondary mailbox folder">
                                            {amigos.map((row) => (
                                                <ListItemButton
                                                    selected={selectedIndex == `${row.Usuario.name}`}
                                                    onClick={(event) => handleListItemClick(event, `${row.Usuario.name}`)}
                                                    sx={{ "borderRadius": "6px", marginBottom: "5px" }}
                                                >
                                                    <ListItemText primary={`${row.Nombre} ${row.Apellido}`}
                                                        secondary={`${row.Usuario.name}`}
                                                    />
                                                </ListItemButton>
                                            ))}
                                        </List>
                                    </Box></Item>

                                <Item sx={{ width: "100%" }}><Chat /></Item>

                            </Grid>
                        </Paper>
                    </div>
                </SnackbarProvider>
            </React.Fragment >
        </AllChatContext.Provider>
    );
}