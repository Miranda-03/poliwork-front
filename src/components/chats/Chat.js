import '@fontsource/roboto';
import * as React from 'react';
import axios from 'axios';
import { useState, useEffect, useRef, useContext } from "react";
import { parseJwt } from '../../utils/cookiesUtils';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import CssBaseline from '@mui/material/CssBaseline';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import MuiAlert, { AlertProps } from '@mui/material/Alert';
import { SnackbarProvider, useSnackbar } from 'notistack';
import DeleteIcon from '@mui/icons-material/Delete';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Divider from '@mui/material/Divider';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import ListItemButton from '@mui/material/ListItemButton';
import { FixedSizeList } from 'react-window';
import ImageIcon from '@mui/icons-material/Image';
import WorkIcon from '@mui/icons-material/Work';
import BeachAccessIcon from '@mui/icons-material/BeachAccess';
import ListItemIcon from '@mui/material/ListItemIcon';
import InboxIcon from '@mui/icons-material/Inbox';
import DraftsIcon from '@mui/icons-material/Drafts';
import Grid from '@mui/material/Grid'; // Grid version 1
import SendIcon from '@mui/icons-material/Send';
import { Container } from '@mui/system';
import { AllChatContext } from './AllChats';
import { socket } from '../../utils/webSocketUtils'
import { SportsHockeyRounded, VpnLock } from '@mui/icons-material';
import Chip from '@mui/material/Chip';

let alumnoName;

export default function Chat() {

    const [messages, setMessages] = useState([])
    const [message, setMessage] = useState([])
    const [inputMessage, setInputMessage] = useState("")

    const myRef = React.createRef();
    const inputMessages = React.createRef();

    const { SelectedUser, userClient } = useContext(AllChatContext);

    const user = SelectedUser;
    const UserClient = userClient
    const userNames = [user, UserClient]
    const sortedUserNames = userNames.sort((a, b) => a.localeCompare(b))
    const ChatKey = `${sortedUserNames[0]}:${sortedUserNames[1]}`

    useEffect(() => {
        socket.on("get_msg", (msg) => {
            setMessages(oldArray => [...oldArray, msg])
        });

        return () => {
            socket.off('get_msg');
        };
    }, [])

    useEffect(() => {
        myRef.current.scrollTop = myRef.current.scrollHeight;
    })


    useEffect(() => {
        const options = {
            withCredentials: true
        }
        axios.get(`${process.env.REACT_APP_CURRENT_URL_TO_API}/chat/${ChatKey}`, options)
            .then((data) => {
                if(data.data.message != undefined){
                    setMessages(data.data.message)
                }
            })
            .catch((error) => {
                console.log(`${error}, error`)
            })
    }, [user])

    const enviarMensaje = (e) => {
        e.preventDefault();
        if (inputMessage.length != 0) {
            const msgpack = {
                "key": ChatKey,
                "userSending": UserClient,
                "userReceiving": user,
                "messageBody": inputMessage,
            }
            setInputMessage("")
            setMessages(oldArray => [...oldArray, msgpack])
            socket.emit("send_msg", msgpack)
        }
    }

    return (
        <React.Fragment >
            <SnackbarProvider maxSnack={3}>
                <CssBaseline />
                <Paper elevation={0}
                    sx={{
                        width: "100%",
                        height: "100%"
                    }}>
                    <Grid
                        container
                        direction="column"
                        justifyContent="flex-end"
                        alignItems="center"
                        wrap="nowrap"
                        sx={{ height: "100%" }}
                    >
                        <Grid container
                            direction="column"
                            justifyContent="flex-start"
                            alignItems="center"
                            wrap="nowrap"
                            sx={{ height: "100%", paddingTop: "10px" }}>


                            <Container id="messageContainer" ref={myRef} sx={{ overflow: "auto" }}>
                                {messages.map((value) => {
                                    if (userClient !== value.userReceiving) {
                                        return (<ListItem sx={{ width: "100%" }}>
                                            <Chip sx={{ marginLeft: "auto", fontSize: "17px", maxWidth: "500px", height: "100%", padding: "7px" }} color="primary"
                                                label={<Typography style={{ whiteSpace: 'normal' }}>{value.messageBody}</Typography>} />
                                        </ListItem>)
                                    }
                                    if (value.userSending == user) {
                                        return (<ListItem sx={{ width: "100%" }}>
                                            <Chip sx={{ marginRight: "auto", fontSize: "17px", maxWidth: "500px", height: "100%", padding: "7px" }}
                                                label={<Typography style={{ whiteSpace: 'normal' }}>{value.messageBody}</Typography>} />
                                        </ListItem>)
                                    }
                                })}
                            </Container>


                        </Grid>

                        <Grid item xs={0} sx={{ width: "100%" }}>

                            <Grid
                                container
                                direction="row"
                                justifyContent="center"
                                alignItems="center"
                                sx={{ widht: "100%", margin: "7px", marginBottom: "12px" }}
                            >
                                <Grid item xs={9}>
                                    <TextField
                                        id="outlined-multiline-flexible"
                                        label="Mensaje"
                                        multiline
                                        size="small"
                                        sx={{ width: "100%" }}
                                        value={inputMessage}
                                        onChange={(e) => setInputMessage(e.target.value)}
                                    />
                                </Grid>
                                <Grid item xs={2}>
                                    <Button variant="contained" sx={{ marginLeft: "7px" }} onClick={enviarMensaje} endIcon={<SendIcon />}>
                                        Send
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Paper>

            </SnackbarProvider>
        </React.Fragment >
    );
}