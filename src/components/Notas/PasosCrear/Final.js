import '@fontsource/roboto';
import * as React from 'react';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import dayjs from 'dayjs';
import TextField from '@mui/material/TextField';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import axios from 'axios';
import { useEffect, useState, useContext } from "react";
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Chip from '@mui/material/Chip';
import { useTheme } from '@mui/material/styles';
import { NoteContext } from '../CreateNote'
import Icon from '@mui/material/Icon';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};
/*
export default function Final() {

    return (
        <React.Fragment>
            <Container maxWidth="sm">
                <Box sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}>
                    <Grid container spacing={3} columns={{ xs: 1, sm: 2, md: 2 }}>
                        <Grid item xs={1} sm >
                            <TextField multiline disabled required sx={{ width: "100%" }} id="standard-basic" label="Titulo" variant="standard" onChange={handleTituloChange} value={titulo} />
                        </Grid>
                        <Grid item xs={1} sm >
                            <LocalizationProvider dateAdapter={AdapterDayjs}>
                                <DateTimePicker
                                    renderInput={(props) => <TextField {...props} />}
                                    label="Fecha y hora"
                                    value={fechaHora}
                                    disabled
                                />
                            </LocalizationProvider>
                        </Grid>
                        <Grid item xs={1} sm={2} >
                            <TextField multiline disabled sx={{ width: "100%" }} id="standard-basic" label="Descripción" onChange={handleDescChange} variant="standard" value={descripcion} />
                        </Grid>
                        <Grid item xs={1} sm={2}>
                            <FormControl sx={{ width: "100%" }}>
                                <InputLabel id="demo-multiple-chip-label">Amigos</InputLabel>
                                <Select
                                    labelId="demo-multiple-chip-label"
                                    id="demo-multiple-chip"
                                    multiple
                                    disabled
                                    value={personName}
                                    onChange={handleChange}
                                    input={<OutlinedInput id="select-multiple-chip" label="Amigos" />}
                                    renderValue={(selected) => (
                                        <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
                                            {selected.map((value) => (
                                                <Chip label={value.Nombre} 
                                                />
                                            ))}
                                        </Box>
                                    )}
                                    MenuProps={MenuProps}
                                >
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={1} sm={2}>
                            <FormControl sx={{ width: "100%" }}>
                                <InputLabel id="demo-multiple-chip-label">Recordatorios</InputLabel>
                                <Select
                                    labelId="demo-multiple-chip-label"
                                    id="demo-multiple-chip"
                                    multiple
                                    disabled
                                    value={personName}
                                    onChange={handleChange}
                                    input={<OutlinedInput id="select-multiple-chip" label="Recordatorios" />}
                                    renderValue={(selected) => (
                                        <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
                                            {selected.map((value) => (
                                                <Chip label={value.Nombre} 
                                                />
                                            ))}
                                        </Box>
                                    )}
                                    MenuProps={MenuProps}
                                >
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={1} sm={2}>
                            <FormControl sx={{ width: "100%" }}>
                                <InputLabel id="demo-multiple-chip-label">Reuniones</InputLabel>
                                <Select
                                    labelId="demo-multiple-chip-label"
                                    id="demo-multiple-chip"
                                    multiple
                                    disabled
                                    value={personName}
                                    onChange={handleChange}
                                    input={<OutlinedInput id="select-multiple-chip" label="Reuniones" />}
                                    renderValue={(selected) => (
                                        <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
                                            {selected.map((value) => (
                                                <Chip label={value.Nombre} 
                                                />
                                            ))}
                                        </Box>
                                    )}
                                    MenuProps={MenuProps}
                                >
                                </Select>
                            </FormControl>
                        </Grid>
                    </Grid>
                </Box>
            </Container>
        </React.Fragment>
    );

}*/