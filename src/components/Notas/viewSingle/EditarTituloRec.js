import '@fontsource/roboto';
import * as React from 'react';
import { useEffect, useState, useContext, useRef, createContext } from "react";
import { ViewNotaContext } from './viewSingle';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import EditIcon from '@mui/icons-material/Edit';
import IconButton from '@mui/material/IconButton';
import Button from '@mui/material/Button';
import MateriasSelect from './materiasSelect'

export const EditarRecContext = createContext([{}, () => { }]);

export default function EditRec() {
    
    const { notaMod, EditRec,  IndexRecEdit} = useContext(ViewNotaContext);
    
    const [openRecEdit, setOpenRecEdit] = EditRec
    const [nota, setNota] = notaMod
    const [indexRec, setIndexRec] = IndexRecEdit
    const [materias, setMaterias] = useState([])

    const values = {
        "mat2" : [materias, setMaterias]
    }

    const handleClickOpenRec = () => {
        openRecEdit(true);
    };

    const handleSendRec = (t) => {
        const notaNew = nota
        
        notaNew.Recordatorios[indexRec].DescripcionReunion = t
        notaNew.Recordatorios[indexRec].Materia = materias
        setNota(notaNew)
        setOpenRecEdit(false);
    };

    const handleCloseRec = () => {
        setOpenRecEdit(false);
    };

    return <EditarRecContext.Provider value={values}>
          <Dialog open={openRecEdit} onClose={handleCloseRec}>
                <DialogTitle>Editar</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Agregar reunion
                    </DialogContentText>
                    <TextField
                        autoFocus
                        multiline
                        margin="dense"
                        id="desc"
                        label="Descripción"
                        type="text"
                        fullWidth
                        variant="standard"
                        defaultValue={ nota.Recordatorios == undefined ? ""  : nota.Recordatorios[indexRec].DescripcionReunion}
                    />

                    <MateriasSelect/>
                    
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseRec}>Cancel</Button>
                    <Button onClick={() => {
                        handleSendRec(document.getElementById('desc').value)
                    }}>Modificar</Button>
                </DialogActions>
            </Dialog>
    </EditarRecContext.Provider >;

}