import * as React from 'react';
import { styled } from '@mui/material/styles';
import Chip from '@mui/material/Chip';
import Paper from '@mui/material/Paper';
import { useEffect, useState, useContext, useRef, createContext } from "react";
import dayjs from 'dayjs';
import TextField from '@mui/material/TextField';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import {agregarFechaContext} from './AgregarFecha'



const ListItem = styled('li')(({ theme }) => ({
    margin: theme.spacing(0.5),
}));


export default function FechasRecordar() {

    const { fechaRec } = useContext(agregarFechaContext)
    const [fechasRec, setFechasRec] = fechaRec
    const [fechas, setFechas] = useState([])
    const [value, setValue] = React.useState(dayjs());


    const handleDeleteObj = (chipToDelete) => () => {
        setFechasRec((chips) => chips.filter((chip) => chip !== chipToDelete));
    };

    const handleAddFechaRec = () => {
        const f = new Date(value)
        setFechasRec(state => ({ ...state }))
        setFechasRec(fechasRec.concat({"date" : `${f.toDateString()} ${f.getHours()}:${f.getMinutes()}:${f.getSeconds()}`}))
        console.log(fechasRec)
    }

    return (
        <React.Fragment>

            <Grid item xs={1} sm={2} sx={{
                marginBottom: "20px"
            }}>
                <Paper
                    sx={{
                        display: 'flex',
                        justifyContent: 'center',
                        flexWrap: 'wrap',
                        listStyle: 'none',
                        p: 0.5,
                        m: 0,
                        marginBottom: "15px"
                    }}
                    component="ul"
                >
                    {fechasRec.map((data, index) => {
                        return (
                            <ListItem key={index}>
                                <Chip
                                    label={`Nueva fecha (${data.date})`}
                                    onDelete={handleDeleteObj(data)}
                                />
                            </ListItem>
                        );
                    })}
                </Paper>
                <Paper
                    sx={{
                        display: 'flex',
                        justifyContent: 'center',
                        flexWrap: 'wrap',
                        listStyle: 'none',
                        p: 0.5,
                        m: 0,
                        marginBottom: "15px"
                    }}
                    component="ul"
                >
                    {fechas.map((data) => {
                        return (
                            <ListItem key={data.$s}>
                                <Chip
                                    label={data.date.toString()}
                                   
                                />
                            </ListItem>
                        );
                    })}
                </Paper>
                <Grid container
                    justifyContent="space-between"
                    alignItems="center"
                    direction="row"
                >
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                        <DateTimePicker
                            renderInput={(props) => <TextField {...props} />}
                            label="DateTimePicker"
                            onChange={(newValue) => {
                                setValue(newValue);
                            }}
                        />
                    </LocalizationProvider>
                    <Button
                        onClick={handleAddFechaRec}
                        size="small">
                        Agregar fecha
                    </Button>
                </Grid>
                <Button
                    onClick={handleAddFechaRec}
                    size="small">
                    Agregar fecha recordar
                </Button>
            </Grid>

        </React.Fragment>
    );
}
