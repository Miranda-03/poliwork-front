import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { Link } from 'react-router-dom';



export default function SingleNote({ titulo, desc, estado, idNota }) {
  return (
    <Card sx={{ minWidth: 300 }}>
      <CardContent>
        <Typography variant="h5" component="div">
          {titulo}
        </Typography>
        <Typography sx={{ mb: 1.5 }} color="text.secondary">
          {estado}
        </Typography>
        <Typography variant="body2">
          {desc}
        </Typography>
      </CardContent>
      <CardActions>
        <Button LinkComponent={Link} to={`/vernota/${idNota}`} size="small">VER</Button>
      </CardActions>
    </Card>
  );
}
