import { BrowserRouter, Routes, Route, HashRouter } from "react-router-dom";
import Home from "./Home";
import SignIn from './SignIn';
import SignUp from './SignUp'
import PrimarySearchAppBar from './appbar'
import ViewAllNotes from './Notas/viewAll'
import CrearNota from './Notas/CreateNote'
import VerAmigos from "./Amigos/VerAmigos";
import SolicitudesViewAll from './Solicitudes/viewAll'
import NotificacionesViewAll from './Notificaciones/viewAll'
import ViewSingle from './Notas/viewSingle/viewSingle'
import AgregarMaterias from './Materias/viewAll'
import AgregarM from './Materias/AgregarMateria'
import EliminarMateria from './Materias/EliminarMateria.JS'
import AllChats from "./chats/AllChats";

export default function Router() {

    return (
        <HashRouter>
            <Routes>
                <Route path="/" element={<PrimarySearchAppBar />}>
                    <Route index element={<Home />} />
                    <Route path="/notas" element={<ViewAllNotes/>} />
                    <Route path="/crearNotas" element={<CrearNota/>} />
                    <Route path="/veramigos" element={<VerAmigos />} />
                    <Route path='/solicitudes' element={<SolicitudesViewAll />} />
                    <Route path='/notificaciones' element={<NotificacionesViewAll />} />
                    <Route path='vernota/:id' element={<ViewSingle/>} />
                    <Route path="/agregarMateria" element={<AgregarMaterias/>}/>
                    <Route path="/agregar" element={<AgregarM/>}/>
                    <Route path="/eliminar" element={<EliminarMateria/>}/>
                    <Route path="/chat" element={<AllChats/>}/>
                </Route>
                <Route path="/login" element={<SignIn />} />
                <Route path="/singup" element={<SignUp />} />
            </Routes>
        </HashRouter>
    )
}