
import os
from selenium import webdriver
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.safari.options import Options as SafariOptions
from selenium.webdriver.edge.options import Options as EdgeOptions
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import Select
from threading import Thread
import time 


BROWSERSTACK_USERNAME = os.environ.get("BROWSERSTACK_USERNAME") or "martnmiranda_nBdHPX"
BROWSERSTACK_ACCESS_KEY = os.environ.get("BROWSERSTACK_ACCESS_KEY") or "kiyXF1ubqhXmAhBsidzT"
URL = os.environ.get("URL") or "https://hub.browserstack.com/wd/hub"
BUILD_NAME = "browserstack-build-1"
capabilities = [
    {
        "browserName": "Chrome",
        "browserVersion": "103.0",
        "os": "Windows",
        "osVersion": "11",
        "sessionName": "BStack Python sample parallel", # test name
        "buildName": BUILD_NAME,  # Your tests will be organized within this build
    },
    {
        "browserName": "Firefox",
        "browserVersion": "102.0",
        "os": "Windows",
        "osVersion": "10",
        "sessionName": "BStack Python sample parallel",
        "buildName": BUILD_NAME,
    },
    {
        "browserName": "Safari",
        "browserVersion": "14.1",
        "os": "OS X",
        "osVersion": "Big Sur",
        "sessionName": "BStack Python sample parallel",
        "buildName": BUILD_NAME,
    },
]
def get_browser_option(browser):
    switcher = {
        "chrome": ChromeOptions(),
        "firefox": FirefoxOptions(),
        "edge": EdgeOptions(),
        "safari": SafariOptions(),
    }
    return switcher.get(browser, ChromeOptions())
def run_session(cap):
    bstack_options = {
        "osVersion" : cap["osVersion"],
        "buildName" : cap["buildName"],
        "sessionName" : cap["sessionName"],
        "userName": BROWSERSTACK_USERNAME,
        "accessKey": BROWSERSTACK_ACCESS_KEY
    }
    if "os" in cap:
      bstack_options["os"] = cap["os"]
    options = get_browser_option(cap["browserName"].lower())
    if "browserVersion" in cap:
      options.browser_version = cap["browserVersion"]
    options.set_capability('bstack:options', bstack_options)
    driver = webdriver.Remote(
        command_executor=URL,
        options=options)
    try:
        driver.get("https://poliwork.onrender.com/")
        driver.maximize_window()
        delay = 20
        myElem = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, 'LOGIN_BUTTON')))
        login = driver.find_element(By.ID, "LOGIN_BUTTON")
        login.click()
        singin_page = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, 'singin-titulo')))
        singup_link = driver.find_element(By.ID, "SINGUP_LINK")
        singup_link.click()
        myElem = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, 'demo-simple-select')))
        select = driver.find_element(By.ID, "demo-simple-select") 
        select.click()
        time.sleep(3)
        curso = driver.find_element(By.ID, "1B")
        curso.click()
        nombreAlumno = driver.find_element(By.ID, "firstName")
        nombreAlumno.send_keys("Selenium")
        apellidoAlumno = driver.find_element(By.ID, "lastName")
        apellidoAlumno.send_keys("Test")
        userName = driver.find_element(By.ID, "name")
        userName.send_keys("selenium-03")
        mail = driver.find_element(By.ID, "email")
        mail.send_keys("test@gmail.com")
        password = driver.find_element(By.ID, "outlined-adornment-password")
        password.send_keys("123")
        boton_singup = driver.find_element(By.ID, "boton-singup")
        time.sleep(2)
        boton_singup.click()
        myElem = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, 'LOGIN_BUTTON')))
        login2 = driver.find_element(By.ID, "LOGIN_BUTTON")
        login2.click()
        userMail = driver.find_element(By.ID, "email")
        userMail.send_keys("test@gmail.com")
        userPass = driver.find_element(By.ID, "password")
        userPass.send_keys("123")
        time.sleep(2)
        boton_singin  = driver.find_element(By.ID, "singin-button")
        boton_singin.click()
        myElem = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, 'boton-usuario-opciones')))
        userOptions = driver.find_element(By.ID, "boton-usuario-opciones")
        userOptions.click()
        myElem = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, 'boton-eliminar-cuenta')))
        boton_dialog = driver.find_element(By.ID, "boton-eliminar-cuenta")
        boton_dialog.click()
        time.sleep(2)
        myElem = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, 'eliminar-cuenta')))
        boton_eliminar_cuenta = driver.find_element(By.ID, "eliminar-cuenta")
        boton_eliminar_cuenta.click()
        singin_finish = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, 'singin-titulo')))
        if singin_page == singin_finish:
            driver.execute_script(
            'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"Passed", "reason": "Se creo y eliminó la cuenta"}}')
    except NoSuchElementException:
        driver.execute_script(
            'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Some elements failed to load"}}')
    except Exception:
        driver.execute_script(
            'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Some exception occurred"}}')

    driver.quit()

for cap in capabilities:
  Thread(target=run_session, args=(cap,)).start()
